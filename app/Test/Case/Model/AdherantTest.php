<?php
App::uses('Adherant', 'Model');

/**
 * Adherant Test Case
 *
 */
class AdherantTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.adherant',
		'app.ligues',
		'app.frais',
		'app.users'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Adherant = ClassRegistry::init('Adherant');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Adherant);

		parent::tearDown();
	}

}
