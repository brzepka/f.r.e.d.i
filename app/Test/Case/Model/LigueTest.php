<?php
App::uses('Ligue', 'Model');

/**
 * Ligue Test Case
 *
 */
class LigueTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.ligue'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Ligue = ClassRegistry::init('Ligue');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Ligue);

		parent::tearDown();
	}

}
