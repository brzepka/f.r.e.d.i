<?php
App::uses('AdherantsHasFrai', 'Model');

/**
 * AdherantsHasFrai Test Case
 *
 */
class AdherantsHasFraiTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.adherants_has_frai',
		'app.adherants',
		'app.frais'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->AdherantsHasFrai = ClassRegistry::init('AdherantsHasFrai');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->AdherantsHasFrai);

		parent::tearDown();
	}

}
