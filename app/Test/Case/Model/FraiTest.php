<?php
App::uses('Frai', 'Model');

/**
 * Frai Test Case
 *
 */
class FraiTest extends CakeTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.frai'
	);

/**
 * setUp method
 *
 * @return void
 */
	public function setUp() {
		parent::setUp();
		$this->Frai = ClassRegistry::init('Frai');
	}

/**
 * tearDown method
 *
 * @return void
 */
	public function tearDown() {
		unset($this->Frai);

		parent::tearDown();
	}

}
