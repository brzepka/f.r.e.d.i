<?php
/**
 * FraiFixture
 *
 */
class FraiFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary'),
		'statut' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 45, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'date' => array('type' => 'date', 'null' => true, 'default' => null),
		'motif' => array('type' => 'integer', 'null' => true, 'default' => null),
		'trajet' => array('type' => 'string', 'null' => true, 'default' => null, 'length' => 45, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'coutTrajet' => array('type' => 'integer', 'null' => true, 'default' => null),
		'peages' => array('type' => 'integer', 'null' => true, 'default' => null),
		'repas' => array('type' => 'integer', 'null' => true, 'default' => null),
		'hebergement' => array('type' => 'integer', 'null' => true, 'default' => null),
		'total' => array('type' => 'integer', 'null' => true, 'default' => null),
		'created' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'updated' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'statut' => 'Lorem ipsum dolor sit amet',
			'date' => '2014-06-07',
			'motif' => 1,
			'trajet' => 'Lorem ipsum dolor sit amet',
			'coutTrajet' => 1,
			'peages' => 1,
			'repas' => 1,
			'hebergement' => 1,
			'total' => 1,
			'created' => '2014-06-07 16:44:16',
			'updated' => '2014-06-07 16:44:16'
		),
	);

}
