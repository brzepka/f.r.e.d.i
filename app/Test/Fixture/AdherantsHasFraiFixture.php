<?php
/**
 * AdherantsHasFraiFixture
 *
 */
class AdherantsHasFraiFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'adherants_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary'),
		'frais_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'key' => 'primary'),
		'indexes' => array(
			'PRIMARY' => array('column' => array('adherants_id', 'frais_id'), 'unique' => 1),
			'fk_adherants_has_frais_frais1_idx' => array('column' => 'frais_id', 'unique' => 0),
			'fk_adherants_has_frais_adherants1_idx' => array('column' => 'adherants_id', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'adherants_id' => 1,
			'frais_id' => 1
		),
	);

}
