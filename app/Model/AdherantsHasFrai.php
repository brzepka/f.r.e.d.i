<?php
App::uses('AppModel', 'Model');
/**
 * AdherantsHasFrai Model
 *
 * @property Adherants $Adherants
 * @property Frais $Frais
 */
class AdherantsHasFrai extends AppModel {

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'adherants_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'frais_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Adherants' => array(
			'className' => 'Adherants',
			'foreignKey' => 'adherants_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		),
		'Frais' => array(
			'className' => 'Frais',
			'foreignKey' => 'frais_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
