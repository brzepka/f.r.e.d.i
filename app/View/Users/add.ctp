<div class="mywell users form">
<?php echo $this->Form->create('User'); ?>
	<fieldset>
		<legend><?php echo __('Ajouter un Utilisateur'); ?></legend>
        <div class="row">
            <div class="col-lg-6">
                <?php
                echo $this->Form->input('username', array('label'=>'Login', 'class'=>'form-control'));
                echo $this->Form->input('password', array('label'=>'Mot de Passe', 'class'=>'form-control'));
                $o = array('admin'=>'Administrateur','user'=>'Utilisateur');
                echo $this->Form->input('role', array('label'=>'Privilèges', 'class'=>'form-control', 'options' => $o, 'empty'=>'Selectionnez'));
                ?>
            </div>
            <div class="col-lg-6">
                <?php
                    echo $this->Form->input('surname', array('label'=>'Nom', 'class'=>'form-control'));
                    echo $this->Form->input('name', array('label'=>'Prénom', 'class'=>'form-control'));
                    echo $this->Form->input('adress', array('label'=>'Adresse', 'class'=>'form-control'));
                    echo $this->Form->input('cp', array('label'=>'CP', 'class'=>'form-control'));
                    echo $this->Form->input('city', array('label'=>'Ville', 'class'=>'form-control'));
                    echo $this->Form->input('rep', array('label'=>'Représentant', 'class'=>'form-control'));
                ?>
            </div>
        </div>
	</fieldset>
    <div class="actions">
        <ul>

            <li><?php echo $this->Html->link(__('Lister les utilisateurs'), array('action' => 'index')); ?></li>
        </ul>
    </div>
<br>
<?php echo $this->Form->submit('Valider', array('class'=>'btn btn-primary')); ?>
<?php echo $this->Form->end(); ?>
</div>

