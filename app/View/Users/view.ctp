<div class="mywell users view">
<legend><?php echo __('Utilisateur'); ?></legend>
    <div class="row">
        <div class="col-lg-6">
            <?php
                echo $this->Form->input('', array('label'=>'Login', 'class'=>'form-control', 'value'=> $user['User']['username'], 'disabled'=>'disabled'));
                $o = array('admin'=>'Administrateur','user'=>'Utilisateur');
                echo $this->Form->input('', array('label'=>'Privilèges', 'class'=>'form-control', 'value'=> $user['User']['role'], 'options'=> $o, 'disabled'=>'disabled'));
                echo $this->Form->input('', array('label'=>'Crée le', 'class'=>'form-control', 'value'=> $user['User']['created'], 'disabled'=>'disabled'));
            ?>
        </div>
        <div class="col-lg-6">
            <?php
                echo $this->Form->input('', array('label'=>'Nom', 'class'=>'form-control', 'value'=> $user['User']['surname'], 'disabled'=>'disabled'));
                echo $this->Form->input('', array('label'=>'Prénom', 'class'=>'form-control', 'value'=> $user['User']['name'], 'disabled'=>'disabled'));
                echo $this->Form->input('', array('label'=>'Adresse', 'class'=>'form-control', 'value'=> $user['User']['adress'], 'disabled'=>'disabled'));
                echo $this->Form->input('', array('label'=>'CP', 'class'=>'form-control', 'value'=> $user['User']['cp'], 'disabled'=>'disabled'));
                echo $this->Form->input('', array('label'=>'Ville', 'class'=>'form-control', 'value'=> $user['User']['city'], 'disabled'=>'disabled'));

            ?>
        </div>
    </div>
    <br>
    <div class="actions">
        <ul>
            <li><?php echo $this->Html->link(__('Editer'), array('action' => 'edit', $user['User']['id'])); ?> </li>
            <li><?php echo $this->Form->postLink(__('Supprimer'), array('action' => 'delete', $user['User']['id']), null, __('Are you sure you want to delete # %s?', $user['User']['id'])); ?> </li>
            <li><?php echo $this->Html->link(__('Lister les Utilisateurs'), array('action' => 'index')); ?> </li>
            <li><?php echo $this->Html->link(__('Ajouter un Utilisateur'), array('action' => 'add')); ?> </li>
        </ul>
    </div>
    <br>
</div>

