<div class="mywell users index">
	<legend><?php echo __('Utilisateurs'); ?></legend>
	<table class="table table-bordered" cellpadding="0" cellspacing="0">
	<tr class="info">
			<th><?php echo $this->Paginator->sort('surname', 'Nom'); ?></th>
			<th><?php echo $this->Paginator->sort('name', 'Prénom'); ?></th>
			<th><?php echo $this->Paginator->sort('city', 'Ville'); ?></th>
			<th><?php echo $this->Paginator->sort('created', 'Crée le'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($users as $user): ?>
	<tr>
		<td><?php echo h($user['User']['surname']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['name']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['city']); ?>&nbsp;</td>
		<td><?php echo h($user['User']['created']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('Voir'), array('action' => 'view', $user['User']['id'])); ?>
			<?php echo $this->Html->link(__('Editer'), array('action' => 'edit', $user['User']['id'])); ?>
			<?php echo $this->Form->postLink(__('Supprimer'), array('action' => 'delete', $user['User']['id']), null, __('Are you sure you want to delete # %s?', $user['User']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
    <div class="actions">
        <ul>
            <li><?php echo $this->Html->link(__('Ajouter un Utilisateur'), array('action' => 'add')); ?></li>
        </ul>
    </div>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="pager">
	<?php
		echo $this->Paginator->prev('< ' . __('Précédent '), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ' '));
		echo $this->Paginator->next(__(' Suivant') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>

