<div class="mywell users form">
<?php echo $this->Form->create('User'); ?>
	<fieldset>
		<legend><?php echo __('Editer un Utilisateur'); ?></legend>
        <div class="row">
            <div class="col-lg-6">
                <?php
                    echo $this->Form->input('id', array('type' =>'hidden'));
                    echo $this->Form->input('username', array('label'=>'Login', 'class'=>'form-control'));
                    echo $this->Form->input('password', array('label'=>'Mot de Passe', 'class'=>'form-control'));
                    echo $this->Form->input('role', array('label'=>'Privilèges', 'class'=>'form-control'));
                ?>
            </div>
            <div class="col-lg-6">
                <?php
                    echo $this->Form->input('surname', array('label'=>'Nom', 'class'=>'form-control'));
                    echo $this->Form->input('name', array('label'=>'Prénom', 'class'=>'form-control'));
                    echo $this->Form->input('adress', array('label'=>'Adresse', 'class'=>'form-control'));
                    echo $this->Form->input('cp', array('label'=>'CP', 'class'=>'form-control'));
                    echo $this->Form->input('city', array('label'=>'Ville', 'class'=>'form-control'));
                ?>
            </div>
        </div>
	</fieldset>
<?php echo $this->Form->submit('Valider', array('class'=>'btn btn-primary')); ?>
<?php echo $this->Form->end(); ?>
    <br>
    <div class="actions">
        <ul>
            <li><?php echo $this->Form->postLink(__('Supprimer'), array('action' => 'delete', $this->Form->value('User.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('User.id'))); ?></li>
            <li><?php echo $this->Html->link(__('Lister les Utilisateurs'), array('action' => 'index')); ?></li>
        </ul>
    </div>
</div>

