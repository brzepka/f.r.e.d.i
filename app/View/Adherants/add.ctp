<div class="mywell adherants form">
<?php echo $this->Form->create('Adherant'); ?>
	<fieldset>
		<legend><?php echo __('Ajouter un Adherant'); ?></legend>
    <div class="row">
        <div class="col-lg-6">
            <?php
                echo $this->Form->input('surname', array('label'=>'Nom','class'=>'form-control'));
                echo $this->Form->input('name', array('label'=>'Prénom','class'=>'form-control'));
                echo $this->Form->input('adress', array('label'=>'Adresse','class'=>'form-control'));
                echo $this->Form->input('cp', array('label'=>'CP','class'=>'form-control'));
                echo $this->Form->input('city', array('label'=>'Ville','class'=>'form-control'));
            ?>
        </div>
        <div class="col-lg-6">
            <?php
                $o = array('0'=>'Masculin', '1'=>'Féminin');
                echo $this->Form->input('sex', array('label'=>'Sexe', 'options' => $o, 'empty' => 'Sélectionnez','class'=>'form-control'));
                echo $this->Form->input('birth', array('label'=>'Date de Naissance','class'=>'form-control'));
                echo $this->Form->input('license', array('label'=>'N° de License','class'=>'form-control'));
                echo $this->Form->input('ligues_id', array('class'=>'form-control'));
            ?>
        </div>
    </div>
	</fieldset>
    <br>
    <?php echo $this->Form->submit('Valider', array('class'=>'btn btn-primary')); ?>
    <?php echo $this->Form->end(); ?>
    <br>
    <div class="actions">
        <ul>
            <li><?php echo $this->Html->link(__('Lister les Adherants'), array('action' => 'index')); ?></li>
            <li><?php echo $this->Html->link(__('Lister les Ligues'), array('controller' => 'ligues', 'action' => 'index')); ?> </li>
            <li><?php echo $this->Html->link(__('Ajouter une Ligue'), array('controller' => 'ligues', 'action' => 'add')); ?> </li>
            <li><?php echo $this->Html->link(__('Lister les Frais'), array('controller' => 'frais', 'action' => 'index')); ?> </li>
            <li><?php echo $this->Html->link(__('Lister les Utilisateurs'), array('controller' => 'users', 'action' => 'index')); ?> </li>
            <li><?php echo $this->Html->link(__('Ajouter un Utilisateur'), array('controller' => 'users', 'action' => 'add')); ?> </li>
        </ul>
    </div>
</div>

