<div class="mywell adherants view">
<legend><?php echo __('Adherant'); ?></legend>
    <div class="row">
        <div class="col-lg-6">
            <?php
                echo $this->Form->input('', array('label'=>'Nom','class'=>'form-control','value'=> $adherant['Adherant']['surname']));
                echo $this->Form->input('', array('label'=>'Prénom','class'=>'form-control', 'value'=> $adherant['Adherant']['surname']));
                echo $this->Form->input('', array('label'=>'Sexe','class'=>'form-control', 'value'=> $adherant['Adherant']['sex']));
            ?>
        </div>
        <div class="col-lg-6">

        </div>
    </div>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($adherant['Adherant']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Surname'); ?></dt>
		<dd>
			<?php echo h($adherant['Adherant']['surname']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Name'); ?></dt>
		<dd>
			<?php echo h($adherant['Adherant']['name']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Sex'); ?></dt>
		<dd>
			<?php echo h($adherant['Adherant']['sex']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Birth'); ?></dt>
		<dd>
			<?php echo h($adherant['Adherant']['birth']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('License'); ?></dt>
		<dd>
			<?php echo h($adherant['Adherant']['license']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Adress'); ?></dt>
		<dd>
			<?php echo h($adherant['Adherant']['adress']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Cp'); ?></dt>
		<dd>
			<?php echo h($adherant['Adherant']['cp']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('City'); ?></dt>
		<dd>
			<?php echo h($adherant['Adherant']['city']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Ligues'); ?></dt>
		<dd>
			<?php echo $this->Html->link($adherant['Ligues']['name'], array('controller' => 'ligues', 'action' => 'view', $adherant['Ligues']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Users'); ?></dt>
		<dd>
			<?php echo $this->Html->link($adherant['Users']['name'], array('controller' => 'users', 'action' => 'view', $adherant['Users']['id'])); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Adherant'), array('action' => 'edit', $adherant['Adherant']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Adherant'), array('action' => 'delete', $adherant['Adherant']['id']), null, __('Are you sure you want to delete # %s?', $adherant['Adherant']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Adherants'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Adherant'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Ligues'), array('controller' => 'ligues', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Ligues'), array('controller' => 'ligues', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Frais'), array('controller' => 'frais', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Frais'), array('controller' => 'frais', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Users'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>
