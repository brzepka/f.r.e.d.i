<div class="adherants form">
<?php echo $this->Form->create('Adherant'); ?>
	<fieldset>
		<legend><?php echo __('Edit Adherant'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('surname');
		echo $this->Form->input('name');
		echo $this->Form->input('sex');
		echo $this->Form->input('birth');
		echo $this->Form->input('license');
		echo $this->Form->input('adress');
		echo $this->Form->input('cp');
		echo $this->Form->input('city');
		echo $this->Form->input('ligues_id');
		echo $this->Form->input('users_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Adherant.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Adherant.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Adherants'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Ligues'), array('controller' => 'ligues', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Ligues'), array('controller' => 'ligues', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Frais'), array('controller' => 'frais', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Frais'), array('controller' => 'frais', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Users'), array('controller' => 'users', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Users'), array('controller' => 'users', 'action' => 'add')); ?> </li>
	</ul>
</div>
