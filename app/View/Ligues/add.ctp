<div class="mywell ligues form">
<?php echo $this->Form->create('Ligue'); ?>
	<fieldset>
		<legend><?php echo __('Ajouter une Ligue'); ?></legend>
	<?php
		echo $this->Form->input('name', array('label'=>'Nom','class'=>'form-control'));
	?>
	</fieldset>
    <br>
    <div class="actions">
        <ul>

            <li><?php echo $this->Html->link(__('Lister les Ligues'), array('action' => 'index')); ?></li>
        </ul>
    </div>

    <?php echo $this->Form->submit('Valider', array('class'=>'btn btn-primary')) ?>
<?php echo $this->Form->end(); ?>
</div>
