<div class="mywell ligues view">
<legend><?php echo __('Ligue'); ?></legend>
    <?php
        echo $this->Form->input('', array('label'=>'Nom', 'class'=>'form-control', 'value' => $ligue["Ligue"]["name"], 'disabled'=>'disabled'));
    ?>
    <br>
    <div class="actions">
        <ul>
            <li><?php echo $this->Html->link(__('Editer la Ligue'), array('action' => 'edit', $ligue['Ligue']['id'])); ?> </li>
            <li><?php echo $this->Form->postLink(__('Supprimer la Ligue'), array('action' => 'delete', $ligue['Ligue']['id']), null, __('Are you sure you want to delete # %s?', $ligue['Ligue']['id'])); ?> </li>
            <li><?php echo $this->Html->link(__('Lister les Ligues'), array('action' => 'index')); ?> </li>
            <li><?php echo $this->Html->link(__('Ajouter une Ligue'), array('action' => 'add')); ?> </li>
        </ul>
    </div>
</div>

