<div class="mywell ligues form">
<?php echo $this->Form->create('Ligue'); ?>
	<fieldset>
		<legend><?php echo __('Editer la Ligue'); ?></legend>
	<?php
		echo $this->Form->input('id');
		echo $this->Form->input('name', array('label'=>'Nom','class'=>'form-control'));
	?>
	</fieldset>
    <br>
    <div class="actions">
        <ul>
            <li><?php echo $this->Form->postLink(__('Supprimer'), array('action' => 'delete', $this->Form->value('Ligue.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Ligue.id'))); ?></li>
            <li><?php echo $this->Html->link(__('Lister les Ligues'), array('action' => 'index')); ?></li>
        </ul>
    </div>
    <br>
<?php echo $this->Form->submit('Valider', array('class' => 'btn btn-primary')); ?>
<?php echo $this->Form->end(); ?>
</div>

