<div class="mywell ligues index">
	<legend><?php echo __('Ligues'); ?></legend>
	<table class="table table-bordered" cellpadding="0" cellspacing="0">
	<tr class="info">
			<th><?php echo $this->Paginator->sort('id'); ?></th>
			<th><?php echo $this->Paginator->sort('name'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($ligues as $ligue): ?>
	<tr>
		<td><?php echo h($ligue['Ligue']['id']); ?>&nbsp;</td>
		<td><?php echo h($ligue['Ligue']['name']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('Voir'), array('action' => 'view', $ligue['Ligue']['id'])); ?>
			<?php echo $this->Html->link(__('Editer'), array('action' => 'edit', $ligue['Ligue']['id'])); ?>
			<?php echo $this->Form->postLink(__('Supprimer'), array('action' => 'delete', $ligue['Ligue']['id']), null, __('Are you sure you want to delete # %s?', $ligue['Ligue']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
    <div class="actions">
        <ul>
            <li><?php echo $this->Html->link(__('Ajouter une Ligue'), array('action' => 'add')); ?></li>
        </ul>
    </div>

    <p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="pager">
	<?php
		echo $this->Paginator->prev('< ' . __('Précédent '), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__(' Suivant') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
