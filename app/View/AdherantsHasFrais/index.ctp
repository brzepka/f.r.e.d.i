<div class="adherantsHasFrais index">
	<h2><?php echo __('Adherants Has Frais'); ?></h2>
	<table cellpadding="0" cellspacing="0">
	<tr>
			<th><?php echo $this->Paginator->sort('adherants_id'); ?></th>
			<th><?php echo $this->Paginator->sort('frais_id'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($adherantsHasFrais as $adherantsHasFrai): ?>
	<tr>
		<td>
			<?php echo $this->Html->link($adherantsHasFrai['Adherants']['name'], array('controller' => 'adherants', 'action' => 'view', $adherantsHasFrai['Adherants']['id'])); ?>
		</td>
		<td>
			<?php echo $this->Html->link($adherantsHasFrai['Frais']['id'], array('controller' => 'frais', 'action' => 'view', $adherantsHasFrai['Frais']['id'])); ?>
		</td>
		<td class="actions">
			<?php echo $this->Html->link(__('View'), array('action' => 'view', $adherantsHasFrai['AdherantsHasFrai']['id'])); ?>
			<?php echo $this->Html->link(__('Edit'), array('action' => 'edit', $adherantsHasFrai['AdherantsHasFrai']['id'])); ?>
			<?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $adherantsHasFrai['AdherantsHasFrai']['id']), null, __('Are you sure you want to delete # %s?', $adherantsHasFrai['AdherantsHasFrai']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="paging">
	<?php
		echo $this->Paginator->prev('< ' . __('previous'), array(), null, array('class' => 'prev disabled'));
		echo $this->Paginator->numbers(array('separator' => ''));
		echo $this->Paginator->next(__('next') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('New Adherants Has Frai'), array('action' => 'add')); ?></li>
		<li><?php echo $this->Html->link(__('List Adherants'), array('controller' => 'adherants', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Adherants'), array('controller' => 'adherants', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Frais'), array('controller' => 'frais', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Frais'), array('controller' => 'frais', 'action' => 'add')); ?> </li>
	</ul>
</div>
