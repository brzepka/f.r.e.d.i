<div class="adherantsHasFrais view">
<h2><?php echo __('Adherants Has Frai'); ?></h2>
	<dl>
		<dt><?php echo __('Adherants'); ?></dt>
		<dd>
			<?php echo $this->Html->link($adherantsHasFrai['Adherants']['name'], array('controller' => 'adherants', 'action' => 'view', $adherantsHasFrai['Adherants']['id'])); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Frais'); ?></dt>
		<dd>
			<?php echo $this->Html->link($adherantsHasFrai['Frais']['id'], array('controller' => 'frais', 'action' => 'view', $adherantsHasFrai['Frais']['id'])); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Adherants Has Frai'), array('action' => 'edit', $adherantsHasFrai['AdherantsHasFrai']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Adherants Has Frai'), array('action' => 'delete', $adherantsHasFrai['AdherantsHasFrai']['id']), null, __('Are you sure you want to delete # %s?', $adherantsHasFrai['AdherantsHasFrai']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Adherants Has Frais'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Adherants Has Frai'), array('action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Adherants'), array('controller' => 'adherants', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Adherants'), array('controller' => 'adherants', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Frais'), array('controller' => 'frais', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Frais'), array('controller' => 'frais', 'action' => 'add')); ?> </li>
	</ul>
</div>
