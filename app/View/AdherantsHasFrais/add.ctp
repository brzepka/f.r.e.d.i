<div class="adherantsHasFrais form">
<?php echo $this->Form->create('AdherantsHasFrai'); ?>
	<fieldset>
		<legend><?php echo __('Add Adherants Has Frai'); ?></legend>
	<?php
		echo $this->Form->input('adherants_id');
		echo $this->Form->input('frais_id');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Adherants Has Frais'), array('action' => 'index')); ?></li>
		<li><?php echo $this->Html->link(__('List Adherants'), array('controller' => 'adherants', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Adherants'), array('controller' => 'adherants', 'action' => 'add')); ?> </li>
		<li><?php echo $this->Html->link(__('List Frais'), array('controller' => 'frais', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Frais'), array('controller' => 'frais', 'action' => 'add')); ?> </li>
	</ul>
</div>
