<?php
/**
 *
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$cakeDescription = 'F.R.E.D.I';
?>
<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $cakeDescription ?>:
		<?php echo $title_for_layout; ?>
	</title>
	<?php
		echo $this->Html->meta('icon');

		//echo $this->Html->css('cake.generic');
        echo $this->Html->css('bootstrap');
        echo $this->Html->css('bootstrap');
        echo $this->html->css('default');

        echo $this->Html->script('jquery');
        echo $this->Html->script('bootstrap.min');

		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
	?>
</head>
<body>
    <nav class="navbar navbar-inverse" role="navigation">
        <div class="container-fluid" >
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <a class="navbar-brand" href="#">F.R.E.D.I par M2L</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li><?php echo $this->Html->link(__('Se déconnecter'), array('controller' => 'users', 'action' => 'logout')); ?></li>
                </ul>
            </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
    </nav>
		<div class="container">
            <nav class="navbar navbar-default" role="navigation">
                <div class="container-fluid" >
                    <ul class="nav navbar-nav">
                        <li><?php echo $this->Html->link(__('Accueil'), array('controller' => 'frais', 'action' => 'index', )); ?></li>
                        <li><?php echo $this->Html->link(__('Mes Remboursements'), array('controller' => 'frais', 'action' => 'index', )); ?></li>
                        <li><?php echo $this->Html->link(__('Mon Compte'), array('controller' => 'guests', 'action' => 'view')); ?></li>
                    </ul>
                </div><!-- /.container-fluid -->
            </nav>
            <div>
                <?php echo $this->Session->flash(); ?>

                <?php echo $this->fetch('content'); ?>
            </div>
		</div>
	<?php //echo $this->element('sql_dump'); ?>
</body>
</html>
