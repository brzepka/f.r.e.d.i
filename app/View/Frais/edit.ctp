<div class="mywell frais form">
<?php echo $this->Form->create('Frai'); ?>
	<fieldset>
		<legend><?php echo __('Editer Frais'); ?></legend>
        <?php echo $this->Form->input('id', array('type' => 'hidden')); ?>
        <div class='row'>
            <div class="col-lg-6">
                <?php
                    echo $this->Form->input('date', array('class' => 'form-control', 'type' => 'text'));
                    $o = array('1' => 'voyage', '2' => 'entrainement');
                    echo $this->Form->input('motif', array('class' => 'form-control', 'options' => $o));
                    echo $this->Form->input('trajet', array('class' => 'form-control'));
                    echo $this->Form->input('coutTrajet', array('class' => 'form-control', 'type' => 'text'));
                ?>
            </div>
            <div class="col-lg-6">
                <?php
		            echo $this->Form->input('peages', array('class' => 'form-control', 'type' => 'text'));
                    echo $this->Form->input('repas', array('class' => 'form-control', 'type' => 'text'));
                    echo $this->Form->input('hebergement', array('class' => 'form-control', 'type' => 'text'));
                ?>
            </div>
        </div>
	</fieldset>
<br>
<?php echo $this->Form->submit('Valider', array('class' => 'btn btn-primary')); ?>
<?php echo $this->Form->end(); ?>
</div>
<div class="actions">
	<legend><?php echo __('Actions'); ?></legend>
	<ul>

		<li><?php echo $this->Form->postLink(__('Delete'), array('action' => 'delete', $this->Form->value('Frai.id')), null, __('Are you sure you want to delete # %s?', $this->Form->value('Frai.id'))); ?></li>
		<li><?php echo $this->Html->link(__('List Frais'), array('action' => 'index')); ?></li>
	</ul>
</div>
