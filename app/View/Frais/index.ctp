<div class="mywell frais index">
	<legend><?php echo __('Frais'); ?></legend>
	<table class="table table-bordered" cellpadding="0" cellspacing="0">
	<tr class="info">
			<th style="width: 5%"><?php echo $this->Paginator->sort('statut', 'Statut'); ?></th>
			<th style="width: 15%"><?php echo $this->Paginator->sort('date', 'Date'); ?></th>
            <th><?php echo $this->Paginator->sort('motif', 'Motif'); ?></th>
			<th><?php echo $this->Paginator->sort('trajet', 'Trajet'); ?></th>
			<th style="width: 10%"><?php echo $this->Paginator->sort('total', 'Montant'); ?></th>
			<th style="width: 15%"><?php echo $this->Paginator->sort('created', 'Crée le'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($frais as $frai): ?>
	<tr>
		<td style="width: 5%; background-color: <?php if($frai['Frai']['statut'] == 2){ echo 'green;';} if($frai['Frai']['statut'] == 1){ echo 'orange;';}else{ echo 'red;';} ?>"></td>
		<td style="width: 15%"><?php echo h($frai['Frai']['date']); ?>&nbsp;</td>
		<td><?php echo h($frai['Frai']['motif']); ?>&nbsp;</td>
		<td><?php echo h($frai['Frai']['trajet']); ?>&nbsp;</td>
		<td style="width: 10%"><?php echo h($frai['Frai']['total']); ?>&nbsp;</td>
		<td style="width: 15%"><?php echo h($frai['Frai']['created']); ?>&nbsp;</td>
		<td class="actions">
			<?php echo $this->Html->link(__('Voir'), array('action' => 'view', $frai['Frai']['id'])); ?>
			<?php echo $this->Html->link(__('Editer'), array('action' => 'edit', $frai['Frai']['id'])); ?>
			<?php echo $this->Form->postLink(__('Supprimer'), array('action' => 'delete', $frai['Frai']['id']), null, __('Confirmer la suppression ?')); ?>
		</td>
	</tr>
<?php endforeach; ?>
	</table>
    <ul>
        <li><?php echo $this->Html->link(__('Nouvelle demande'), array('action' => 'add')); ?></li>
    </ul>
    <br>
	<p>
	<?php
	echo $this->Paginator->counter(array(
	'format' => __('Page {:page} of {:pages}, showing {:current} records out of {:count} total, starting on record {:start}, ending on {:end}')
	));
	?>	</p>
	<div class="pager">
	<?php
		echo $this->Paginator->prev('< ' . __('Précédent '), array(), null, array('class' => 'previous disabled'));
		echo $this->Paginator->numbers(array('separator' => ' '));
		echo $this->Paginator->next(__(' Suivant') . ' >', array(), null, array('class' => 'next disabled'));
	?>
	</div>
</div>

