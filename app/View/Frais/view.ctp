<div class="mywell frais view">
<legend><?php echo __('Frais'); ?></legend>
    <div class="row">
        <div class="col-lg-6">
            <?php
                echo $this->Form->input('statut', array('label' => 'Statut', 'class' => 'form-control', 'disabled' =>'disabled', 'value' => $frai['Frai']['statut']));
                echo $this->Form->input('statut', array('label' => 'Date', 'class' => 'form-control', 'disabled' =>'disabled', 'value' => $frai['Frai']['date']));
                echo $this->Form->input('statut', array('label' => 'Motif', 'class' => 'form-control', 'disabled' =>'disabled', 'value' => $frai['Frai']['motif']));
                echo $this->Form->input('statut', array('label' => 'Trajet', 'class' => 'form-control', 'disabled' =>'disabled', 'value' => $frai['Frai']['trajet']));
            ?>
        </div>
        <div class="col-lg-6">
            <?php
                echo $this->Form->input('statut', array('label' => 'Peages', 'class' => 'form-control', 'disabled' =>'disabled', 'value' => $frai['Frai']['peages']));
                echo $this->Form->input('statut', array('label' => 'Hebergement', 'class' => 'form-control', 'disabled' =>'disabled', 'value' => $frai['Frai']['hebergement']));
                echo $this->Form->input('statut', array('label' => 'Repas', 'class' => 'form-control', 'disabled' =>'disabled', 'value' => $frai['Frai']['repas']));
                echo $this->Form->input('statut', array('label' => 'Total', 'class' => 'form-control', 'disabled' =>'disabled', 'value' => $frai['Frai']['total']));
                echo '<br>';
            ?>
        </div>
    </div>
</div>
<div class="actions">
	<legend><?php echo __('Actions'); ?></legend>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Frai'), array('action' => 'edit', $frai['Frai']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Frai'), array('action' => 'delete', $frai['Frai']['id']), null, __('Are you sure you want to delete # %s?', $frai['Frai']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Frais'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Frai'), array('action' => 'add')); ?> </li>
	</ul>
</div>
