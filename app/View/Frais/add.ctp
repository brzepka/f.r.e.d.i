<div class="mywell frais form">
<?php echo $this->Form->create('Frai'); ?>
	<fieldset>
		<legend><?php echo __('Ajout Frais'); ?></legend>
        <div class="row">
            <div class="col-lg-6">
                <?php
                echo $this->Form->input('date', array('class' => 'form-control', 'type' => 'text', 'placeholder' => 'DD/MM/AAAA'));
                $o = array('0' => 'Réunion', '1' => 'Compétition Régionale', '2' => 'Compétition Nationionale', '3' => 'Compétition Internationale', '4' => 'Stage');
                echo $this->Form->input('motif', array('class' => 'form-control', 'options' => $o, 'empty' => 'Selectionnez'));
                echo $this->Form->input('trajet', array('class' => 'form-control', 'type' => 'text'));
                echo $this->Form->input('coutTrajet', array('class' => 'form-control', 'type' => 'text'));
                ?>
            </div>
            <div class="col-lg-6">
                <?php
                    echo $this->Form->input('peages', array('class' => 'form-control', 'type' => 'text'));
                    echo $this->Form->input('repas', array('class' => 'form-control', 'type' => 'text'));
                    echo $this->Form->input('hebergement', array('class' => 'form-control', 'type' => 'text'));
                ?>
            </div>
        </div>
	</fieldset>
<br>
<?php echo $this->Form->submit('Valider', array('class' => 'btn btn-primary')); ?>
<?php echo $this->Form->end(); ?>
</div>
<div class="actions">
	<legend><?php echo __('Actions'); ?></legend>
	<ul>

		<li><?php echo $this->Html->link(__('List Frais'), array('action' => 'index')); ?></li>
	</ul>
</div>
