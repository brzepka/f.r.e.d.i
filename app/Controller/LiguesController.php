<?php
App::uses('AppController', 'Controller');
/**
 * Ligues Controller
 *
 * @property Ligue $Ligue
 * @property PaginatorComponent $Paginator
 */
class LiguesController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Ligue->recursive = 0;
		$this->set('ligues', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Ligue->exists($id)) {
			throw new NotFoundException(__('Invalid ligue'));
		}
		$options = array('conditions' => array('Ligue.' . $this->Ligue->primaryKey => $id));
		$this->set('ligue', $this->Ligue->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Ligue->create();
			if ($this->Ligue->save($this->request->data)) {
				$this->Session->setFlash(__('The ligue has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The ligue could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Ligue->exists($id)) {
			throw new NotFoundException(__('Invalid ligue'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Ligue->save($this->request->data)) {
				$this->Session->setFlash(__('The ligue has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The ligue could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Ligue.' . $this->Ligue->primaryKey => $id));
			$this->request->data = $this->Ligue->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Ligue->id = $id;
		if (!$this->Ligue->exists()) {
			throw new NotFoundException(__('Invalid ligue'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Ligue->delete()) {
			$this->Session->setFlash(__('The ligue has been deleted.'));
		} else {
			$this->Session->setFlash(__('The ligue could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}}
