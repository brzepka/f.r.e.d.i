<?php
App::uses('AppController', 'Controller');
/**
 * AdherantsHasFrais Controller
 *
 * @property AdherantsHasFrai $AdherantsHasFrai
 * @property PaginatorComponent $Paginator
 */
class AdherantsHasFraisController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->AdherantsHasFrai->recursive = 0;
		$this->set('adherantsHasFrais', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->AdherantsHasFrai->exists($id)) {
			throw new NotFoundException(__('Invalid adherants has frai'));
		}
		$options = array('conditions' => array('AdherantsHasFrai.' . $this->AdherantsHasFrai->primaryKey => $id));
		$this->set('adherantsHasFrai', $this->AdherantsHasFrai->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->AdherantsHasFrai->create();
			if ($this->AdherantsHasFrai->save($this->request->data)) {
				$this->Session->setFlash(__('The adherants has frai has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The adherants has frai could not be saved. Please, try again.'));
			}
		}
		$adherants = $this->AdherantsHasFrai->Adherant->find('list');
		$frais = $this->AdherantsHasFrai->Frai->find('list');
		$this->set(compact('adherants', 'frais'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->AdherantsHasFrai->exists($id)) {
			throw new NotFoundException(__('Invalid adherants has frai'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->AdherantsHasFrai->save($this->request->data)) {
				$this->Session->setFlash(__('The adherants has frai has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The adherants has frai could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('AdherantsHasFrai.' . $this->AdherantsHasFrai->primaryKey => $id));
			$this->request->data = $this->AdherantsHasFrai->find('first', $options);
		}
		$adherants = $this->AdherantsHasFrai->Adherant->find('list');
		$frais = $this->AdherantsHasFrai->Frai->find('list');
		$this->set(compact('adherants', 'frais'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->AdherantsHasFrai->id = $id;
		if (!$this->AdherantsHasFrai->exists()) {
			throw new NotFoundException(__('Invalid adherants has frai'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->AdherantsHasFrai->delete()) {
			$this->Session->setFlash(__('The adherants has frai has been deleted.'));
		} else {
			$this->Session->setFlash(__('The adherants has frai could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}}
