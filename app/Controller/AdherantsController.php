<?php
App::uses('AppController', 'Controller');
/**
 * Adherants Controller
 *
 * @property Adherant $Adherant
 * @property PaginatorComponent $Paginator
 */
class AdherantsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Adherant->recursive = 0;
		$this->set('adherants', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Adherant->exists($id)) {
			throw new NotFoundException(__('Invalid adherant'));
		}
		$options = array('conditions' => array('Adherant.' . $this->Adherant->primaryKey => $id));
		$this->set('adherant', $this->Adherant->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
        $this->loadModel('Ligue');
        $this->loadModel('Frai');
        $this->loadModel('User');
		if ($this->request->is('post')) {
			$this->Adherant->create();
			if ($this->Adherant->save($this->request->data)) {
				$this->Session->setFlash(__('The adherant has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The adherant could not be saved. Please, try again.'));
			}
		}
		$ligues = $this->Ligue->find('list');
		$frais = $this->Frai->find('list');
		$users = $this->User->find('list');
		$this->set(compact('ligues', 'frais', 'users'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Adherant->exists($id)) {
			throw new NotFoundException(__('Invalid adherant'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Adherant->save($this->request->data)) {
				$this->Session->setFlash(__('The adherant has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The adherant could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Adherant.' . $this->Adherant->primaryKey => $id));
			$this->request->data = $this->Adherant->find('first', $options);
		}
		$ligues = $this->Adherant->Ligue->find('list');
		$frais = $this->Adherant->Frai->find('list');
		$users = $this->Adherant->User->find('list');
		$this->set(compact('ligues', 'frais', 'users'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Adherant->id = $id;
		if (!$this->Adherant->exists()) {
			throw new NotFoundException(__('Invalid adherant'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Adherant->delete()) {
			$this->Session->setFlash(__('The adherant has been deleted.'));
		} else {
			$this->Session->setFlash(__('The adherant could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}}
