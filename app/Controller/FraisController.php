<?php
App::uses('AppController', 'Controller');
/**
 * Frais Controller
 *
 * @property Frai $Frai
 * @property PaginatorComponent $Paginator
 */
class FraisController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Frai->recursive = 0;
		$this->set('frais', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Frai->exists($id)) {
			throw new NotFoundException(__('Invalid frai'));
		}
		$options = array('conditions' => array('Frai.' . $this->Frai->primaryKey => $id));
		$this->set('frai', $this->Frai->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
            $this->request->data['Frai']['statut'] = '1';
            $this->request->data['Frai']['total'] = $this->request->data['Frai']['coutTrajet'] + $this->request->data['Frai']['peages'] + $this->request->data['Frai']['hebergement'] + $this->request->data['Frai']['repas'];

			$this->Frai->create();
			if ($this->Frai->save($this->request->data)) {
				$this->Session->setFlash(__('The frai has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The frai could not be saved. Please, try again.'));
			}
		}
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Frai->exists($id)) {
			throw new NotFoundException(__('Invalid frai'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Frai->save($this->request->data)) {
				$this->Session->setFlash(__('The frai has been saved.'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The frai could not be saved. Please, try again.'));
			}
		} else {
			$options = array('conditions' => array('Frai.' . $this->Frai->primaryKey => $id));
			$this->request->data = $this->Frai->find('first', $options);
		}
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Frai->id = $id;
		if (!$this->Frai->exists()) {
			throw new NotFoundException(__('Invalid frai'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Frai->delete()) {
			$this->Session->setFlash(__('The frai has been deleted.'));
		} else {
			$this->Session->setFlash(__('The frai could not be deleted. Please, try again.'));
		}
		return $this->redirect(array('action' => 'index'));
	}}
